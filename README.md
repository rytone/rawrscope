# !!! MIGRATED TO GITHUB: [chiptunecafe/rawrscope](https://github.com/chiptunecafe/rawrscope)

rawrscope is a fast, user-friendly, and cross-platform tool for creating
oscilloscope visualizations of audio, typically chiptune.
